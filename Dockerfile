FROM python:3.6
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
ADD . /app
WORKDIR /app
RUN pip install --ignore-installed -r requirements.txt --no-cache-dir
CMD python3 api.py