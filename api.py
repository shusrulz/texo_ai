import os
import json
import itertools
from flask import Flask, request, jsonify


from content_based_recommendation import content_recommendation

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))


@app.route("/recommend_product", methods=["GET", "POST"])
def product_recommendation():
    """API for recommending the relevant products similar to given title
    """
    try:
        input_data = request.json #takes input as json 
        response = {}
        title = input_data["title"]
        try:
            top_recommendations = content_recommendation(title) #method for recommending top contents
            response["top_recommendations"] = top_recommendations
            response['Status'] = "Recommendations Successfully Retrived"
        except:
            response["top_recommendations"] = []
            response["Status"] = "Recommendations Not Retrived"
        return jsonify(response)
    except:
        return jsonify({"top_recommendations":[],"Status": "Error Occured!!"})


if __name__ == '__main__':
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=9015, debug=True)
