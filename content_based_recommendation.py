import pandas as pd 
import os

from sklearn.feature_extraction.text import TfidfVectorizer 
from sklearn.metrics.pairwise import cosine_similarity
from gensim.utils import simple_preprocess
from nltk.corpus import stopwords
import pickle
import numpy as np


product = pd.read_csv('public.product_product.csv')

def content_recommendation(title):
    vectorizer_model = pickle.load(open('vectorizer_model','rb'))
    tf_matrix = pickle.load(open('tf_matrix','rb'))
    final_data = pd.read_csv('final_data.csv')
    product_id = list(final_data['id'].values)
    title = simple_preprocess(title)
    title_matrix = vectorizer_model.transform(title)
    similarity = cosine_similarity(tf_matrix,title_matrix)
    similarity_list = list(np.sum(similarity,axis = 1))
    similar_id = {}
    similar_item = []
    for i in range(5):
        max_sim = max(similarity_list)
        max_sim_idx = similarity_list.index(max_sim)
        id = product_id[max_sim_idx]
        item_name = product[product['id']==id]['title'].values[0]
        similar_item.append(item_name)
        similarity_list.pop(max_sim_idx)
        product_id.pop(max_sim_idx)
    return similar_item

# print(content_recommendation("Exclusive Men's Yeezy Running Shoes"))


