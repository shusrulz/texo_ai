import pandas as pd 
from sklearn.feature_extraction.text import TfidfVectorizer 
import re 
from gensim.utils import simple_preprocess
from nltk.corpus import stopwords
import pickle
'''
This python script is to create a vectorizer model of all the products descriptions

'''
# productClass = pd.read_csv('public.product_productclass.csv')
product = pd.read_csv('public.product_product.csv')
product['final_description'] = product['title']+product['description']
final_data = product[['id','final_description']]
final_data.to_csv('final_data.csv',index = False)
stopwords = set(stopwords.words('english'))

def clean_text(text):
    text = simple_preprocess(text)
    text = [txt for txt in text if not txt in stopwords]
    return ' '.join(text)

final_descritpion = final_data['final_description'].apply(clean_text)
vectorizer = TfidfVectorizer()
tf_matrix = vectorizer.fit_transform(final_descritpion)

pickle.dump(vectorizer,open('vectorizer_model',"wb"))
pickle.dump(tf_matrix,open('tf_matrix','wb'))
